CREATE TABLE IF NOT EXISTS authors (
  id SERIAL PRIMARY KEY,
  date_created TIMESTAMP DEFAULT NOW(),
  first_name VARCHAR(64),
  last_name VARCHAR(64)
);

CREATE TABLE IF NOT EXISTS books (
  id SERIAL PRIMARY KEY,
  author_id INTEGER REFERENCES authors(id),
  date_created TIMESTAMP DEFAULT NOW(),
  title VARCHAR(256)
);
