#### Запуск:

1. Прописываем доступ к PostgreSQL в qmobi_test.py
2. source venv/bin/activate
3. export FLASK_APP=qmobi_test.py (если linux)
4. flask initdb
5. flask run

#### Примеры запросов:


###### Создание автора
`curl -X POST -d '{"first_name":"Alexandr", "last_name":"Pushkin"}' -H "Content-Type: application/json" http://127.0.0.1:5000/authors`


###### Создание книги у автора
`curl -X POST -d '{"title":"Капитанская дочка"}' -H "Content-Type: application/json" http://127.0.0.1:5000/authors/1/books`
`curl -X POST -d '{"title":"Руслан и Людмила"}' -H "Content-Type: application/json" http://127.0.0.1:5000/authors/1/books`


###### Просмотр автора и все его книги
`curl http://127.0.0.1:5000/authors/1`


###### Просмотр, редактирование, удаление книги у автора
`curl http://127.0.0.1:5000/authors/2/books/1`

`curl -X PUT -d '{"title":"Не Руслан и Не Людмила"}' -H "Content-Type: application/json" http://127.0.0.1:5000/authors/1/books/2`

`curl -X DELETE http://127.0.0.1:5000/authors/1/books/2`
