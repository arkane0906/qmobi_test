import psycopg2
from flask import Flask, g, request, json

app = Flask(__name__)

app.config['DEBUG'] = True
app.config['DB_CONFIG'] = 'host=127.0.0.1 port=5432 dbname=qmobi user=postgres password=QmobiPassword'


@app.before_request
def connect_db():
    if not hasattr(g, 'db'):
        g.db = psycopg2.connect(app.config['DB_CONFIG'])


@app.teardown_request
def close_db(e):
    if hasattr(g, 'db'):
        g.db.close()


def init_db():
    db = psycopg2.connect(app.config['DB_CONFIG'])
    with app.open_resource('schema.sql', mode='r') as f:
        db.cursor().execute(f.read())
    db.commit()
    db.close()


@app.cli.command('initdb')
def initdb_command():
    """Инициализация БД"""
    init_db()
    print('БД Инициализирована.')


@app.route('/authors', methods=['POST'])
def author_create():
    """Создание автора"""
    first_name = request.json.get('first_name')
    last_name = request.json.get('last_name')
    cursor = g.db.cursor()
    cursor.execute('''
        INSERT INTO authors (first_name, last_name)
        VALUES (%s, %s)
        RETURNING *;
    ''', (first_name, last_name))
    result = cursor.fetchone()
    g.db.commit()
    return json.dumps({
        'id': result[0],
        'date_created': result[1],
        'first_name': result[2],
        'last_name': result[3]
    }, ensure_ascii=False)


@app.route('/authors/<int:author_id>', methods=['GET'])
def author_get(author_id):
    """Просмотр автора и все его книги"""
    cursor = g.db.cursor()
    cursor.execute('''
        SELECT *
        FROM authors a
        WHERE a.id = %s;
    ''', (author_id,))
    author_result = cursor.fetchone()

    if not author_result:
        return json.dumps({
            'error': 'Author not found'
        }, ensure_ascii=False)

    author = {
        'id': author_result[0],
        'date_created': author_result[1],
        'first_name': author_result[2],
        'last_name': author_result[3],
        'books': []
    }
    cursor.execute('''
        SELECT b.id, b.title, b.date_created
        FROM authors a
        JOIN books b ON a.id = b.author_id
        WHERE a.id = %s;
    ''', (author_id,))
    books_result = cursor.fetchall()
    for book in books_result:
        author['books'].append({
            'id': book[0],
            'title': book[1],
            'date_created': book[2],
        })
    return json.dumps(author, ensure_ascii=False)


@app.route('/authors/<int:author_id>/books', methods=['POST'])
def book_create(author_id):
    """Создание книги у автора"""
    title = request.json.get('title')
    cursor = g.db.cursor()
    cursor.execute('''
        INSERT INTO books (author_id, title)
        VALUES (%s, %s)
        RETURNING *;
    ''', (author_id, title))

    result = cursor.fetchone()
    g.db.commit()

    return json.dumps({
        'id': result[0],
        'author_id': result[1],
        'date_created': result[2],
        'title': result[3]
    }, ensure_ascii=False)


@app.route('/authors/<int:author_id>/books/<int:book_id>', methods=['GET', 'PUT', 'DELETE'])
def book_get_update_delete(author_id, book_id):
    """Просмотр, редактирование, удаление книги у автора"""
    if request.method == 'GET':
        cursor = g.db.cursor()
        cursor.execute('''
            SELECT *
            FROM books b
            WHERE b.id = %s;
        ''', (book_id,))

        book_result = cursor.fetchone()

        if not book_result:
            return json.dumps({
                'error': 'Book not found'
            }, ensure_ascii=False)

        return json.dumps({
            'id': book_result[0],
            'author_id': book_result[1],
            'date_created': book_result[2],
            'title': book_result[3]
        }, ensure_ascii=False)

    elif request.method == 'PUT':
        title = request.json.get('title')
        cursor = g.db.cursor()
        cursor.execute('''
            UPDATE books b
            SET title = %s
            WHERE b.id = %s;
        ''', (title, book_id))

        g.db.commit()

        cursor.execute('''
            SELECT *
            FROM books b
            WHERE b.id = %s;
        ''', (book_id,))

        book_result = cursor.fetchone()

        if not book_result:
            return json.dumps({
                'error': 'Book not found'
            }, ensure_ascii=False)

        return json.dumps({
            'id': book_result[0],
            'author_id': book_result[1],
            'date_created': book_result[2],
            'title': book_result[3]
        }, ensure_ascii=False)

    elif request.method == 'DELETE':
        cursor = g.db.cursor()
        cursor.execute('''
            DELETE
            FROM books b
            WHERE b.id = %s;
        ''', (book_id,))
        g.db.commit()

        return 'ok'


if __name__ == '__main__':
    app.run()
